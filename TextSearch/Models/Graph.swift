//
//  Graph.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/20/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

public class Graph: CustomStringConvertible, Equatable {
  public private(set) var nodes: [Node]
  private var limiter: Int
  public var isFull : Bool {
    return nodes.count == limiter
  }
  private var fulfilledBlock: ()-> Void
  
  public init(limiter: Int, fulfilledBlock: @escaping () -> Void) {
    precondition(limiter > 0)
    
    self.nodes = []
    self.limiter = limiter
    self.fulfilledBlock = fulfilledBlock
  }
  
  public func addNode(_ url: String) -> Node? {
    guard !isFull else {
      fulfilledBlock()
      return nil
    }
    
    let node = Node(url)
    nodes.append(node)

    return node
  }
  
  public func addEdge(_ source: Node, neighbor: Node) {
    let edge = Edge(neighbor)
    source.neighbors.append(edge)
  }
  
  public var description: String {
    var description = ""
    
    for node in nodes {
      if !node.neighbors.isEmpty {
        description += "[node: \(node.url) edges: \(node.neighbors.map { $0.neighbor.url})]"
      }
    }
    
    return description
  }
  
  public func findNodeWithLabel(_ url: String) -> Node {
    return nodes.filter { $0.url == url }.first!
  }
}

public func == (_ lhs: Graph, rhs: Graph) -> Bool {
  return lhs.nodes == rhs.nodes
}
