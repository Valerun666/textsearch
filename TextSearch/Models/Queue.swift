//
//  Queue.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/20/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

public struct Queue<T> {
  private var array: [T]
  
  public init() {
    array = []
  }
  
  public var isEmpty: Bool {
    return array.isEmpty
  }
  
  public var count: Int {
    return array.count
  }
  
  public mutating func enqueue(_ element: T) {
    array.append(element)
  }
  
  public mutating func clear() {
    array.removeAll()
  }
  
  public mutating func dequeue() -> T? {
    if isEmpty {
      return nil
    } else {
      return array.removeFirst()
    }
  }
  
  public func peek() -> T? {
    return array.first
  }
}
