//
//  Node.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/20/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

public enum NodeStatus: String {
  case Loading
  case Found
  case NotFound = "Not Found"
  case Error
}

public class Node: CustomStringConvertible, Equatable {
  public var neighbors: [Edge]
  
  public private(set) var url: String
  public var visited: Bool
  public var status: NodeStatus = .Loading
  public var statusDescription: String?
  public var content: String?
  
  public init(_ url: String) {
    self.url = url
    neighbors = []
    visited = false
  }
  
  public var description: String {
    return "Node(label: \(url), status: \(status)"
  }
  
  public func remove(_ edge: Edge) {
    neighbors.remove(at: neighbors.index { $0 === edge }!)
  }
  
  public func setErrorStatus(errorStatus: String) {
    status = .Error
    statusDescription = errorStatus
  }
}

public func == (_ lhs: Node, rhs: Node) -> Bool {
  return lhs.url == rhs.url && lhs.neighbors == rhs.neighbors
}
