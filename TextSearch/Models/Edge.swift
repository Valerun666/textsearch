//
//  Edge.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/20/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

public class Edge: Equatable {
  public var neighbor: Node
  
  public init(_ neighbor: Node) {
    self.neighbor = neighbor
  }
}

public func == (_ lhs: Edge, rhs: Edge) -> Bool {
  return lhs.neighbor == rhs.neighbor
}
