//
//  ViewController.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/19/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit
import SwiftValidator

class SearchViewController: UIViewController {
  @IBOutlet weak var urlTF: UITextField!
  @IBOutlet weak var searchableTF: UITextField!
  @IBOutlet weak var threadQuantityTF: UITextField!
  @IBOutlet weak var urlLimitTF: UITextField!
  @IBOutlet weak var urlNotificationLbl: UILabel!
  @IBOutlet weak var searchNotificationLbl: UILabel!
  @IBOutlet weak var threadQuantityNotificationLbl: UILabel!
  @IBOutlet weak var urlQuantityNotificationLbl: UILabel!
  let validator = Validator()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupValidator()
  }
  
  fileprivate func setupValidator() {
    validator.registerField(urlTF, errorLabel: urlNotificationLbl, rules: [RequiredRule(), URLRule()])
    validator.registerField(searchableTF, errorLabel: searchNotificationLbl, rules: [RequiredRule(), MinLengthRule(length: 1)])
    validator.registerField(threadQuantityTF, errorLabel: threadQuantityNotificationLbl, rules: [RequiredRule(), ThreadRule()])
    validator.registerField(urlLimitTF, errorLabel: urlQuantityNotificationLbl, rules: [RequiredRule(), URLQuantityRule()])
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == K.ResultSegue {
      let destinationVC = segue.destination as! ResultViewController
      destinationVC.setupData(url: urlTF.text!, searchable: searchableTF.text!, threadQuantity: Int(threadQuantityTF.text!)!, urlQuantity: Int(urlLimitTF.text!)!)
    }
  }
  
  @IBAction func searchAction(_ sender: Any) {
    validator.validate(self)
  }
  
  func textFieldsAreValid() {
    urlTF.layer.borderWidth = 0
    searchableTF.layer.borderWidth = 0
    threadQuantityTF.layer.borderWidth = 0
    urlLimitTF.layer.borderWidth = 0
    
    urlNotificationLbl.isHidden = true
    searchNotificationLbl.isHidden = true
    threadQuantityNotificationLbl.isHidden = true
    urlQuantityNotificationLbl.isHidden = true
  }
}

extension SearchViewController: ValidationDelegate {
  func validationSuccessful() {
    textFieldsAreValid()
    performSegue(withIdentifier: K.ResultSegue, sender: self)
  }
  
  func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
    textFieldsAreValid()
    
    for (field, error) in errors {
      if let field = field as? UITextField {
        field.layer.borderColor = UIColor.red.cgColor
        field.layer.borderWidth = 1.0
      }
      error.errorLabel?.text = error.errorMessage 
      error.errorLabel?.isHidden = false
    }
  }
}

