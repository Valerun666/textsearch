//
//  ResultViewController.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/24/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var startSearchBtn: UIBarButtonItem!
  @IBOutlet weak var stopSearchBtn: UIBarButtonItem!
  @IBOutlet weak var progressLbl: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  private var urlCandidat = ""
  private var searchable = ""
  private var urlLimit = 0
  private var taskLimit = 0
  private weak var graph: Graph?
  private var graphNodeProcessor: GraphProcessor?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    subscribForNotifications()
    initialSetup()
    runProcessor()
  }
  
  func setupData(url: String, searchable: String, threadQuantity: Int, urlQuantity: Int) {
    self.urlCandidat = url
    self.urlLimit = urlQuantity
    self.taskLimit = threadQuantity
    self.searchable = searchable
  }
  
  fileprivate func initialSetup() {
    let operationQueue = OperationQueue()
    operationQueue.maxConcurrentOperationCount = taskLimit
    
    self.graphNodeProcessor = GraphProcessor(operationQueue: operationQueue, searchable: searchable, initialURL: urlCandidat, urlLimit: urlLimit, processFinished: {[weak self] in
      guard let strongSelf = self else {
        return
      }
      
      strongSelf.processFinishedCallback()
    })
    
    graph = graphNodeProcessor?.graph
  }
  
  fileprivate func runProcessor() {
    startSearchBtn.isEnabled = false
    stopSearchBtn.isEnabled = true
    activityIndicator.startAnimating()
    progressLbl.text = "in process..."
    graphNodeProcessor?.startProcess()
  }
  
  fileprivate func processFinishedCallback() {
    startSearchBtn.isEnabled = true
    stopSearchBtn.isEnabled = false
    progressLbl.text = "finished loading"
    activityIndicator.stopAnimating()
    graphNodeProcessor?.stopProcess()
    print(graph)
  }
  
  fileprivate func subscribForNotifications() {
    NotificationCenter.default.addObserver(self, selector: #selector(addedNode(notification:)), name: NSNotification.Name(rawValue: K.NodesAddedNotification), object: nil)
  }
  
  @objc func addedNode(notification: Notification) {
    tableView.reloadData()
  }

  @IBAction func startSearch(_ sender: Any) {
    initialSetup()
    tableView.reloadData()
    runProcessor()
  }
  
  @IBAction func stopSearch(_ sender: Any) {
    startSearchBtn.isEnabled = true
    stopSearchBtn.isEnabled = false
    activityIndicator.stopAnimating()
    progressLbl.text = "process stoped..."
    graphNodeProcessor?.stopProcess()
  }
}

extension ResultViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: K.UrlCell, for: indexPath) as! URLCell
    let node = graph!.nodes[indexPath.row]
    
    cell.urlLbl.text = node.url
    cell.statusLbl.text = node.status == .Error ? node.statusDescription : node.status.rawValue
    
    return cell
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let graph = graph else {
      return 0
    }
    return graph.nodes.count
  }
}
