//
//  URLCell.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/24/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

class URLCell: UITableViewCell {
  @IBOutlet weak var urlLbl: UILabel!
  @IBOutlet weak var statusLbl: UILabel!
  
  override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)

      // Configure the view for the selected state
  }

}
