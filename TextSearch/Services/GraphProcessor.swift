//
//  GraphProcessor.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/23/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

class GraphProcessor {
  var nodeQueue: Queue<Node>?
  var operationQueue: OperationQueue
  var searchable: String
  var initialURL: String
  let urlLimit: Int
  lazy var graph: Graph = {
    let graph = Graph(limiter: urlLimit, fulfilledBlock: { [weak self] in
      guard let strongSelf = self else {
        return
      }
    })
    
    return graph
  }()
  var processFinished: (()->Void)?
  
  init(operationQueue: OperationQueue, searchable: String, initialURL: String, urlLimit: Int, processFinished: @escaping ()->Void) {
    precondition(urlLimit > 0)
    self.operationQueue = operationQueue
    self.searchable = searchable
    self.initialURL = initialURL
    self.urlLimit = urlLimit
    self.processFinished = processFinished
  }
  
  func startProcess() {
    let initialNode = graph.addNode(initialURL)
    
    guard let node = initialNode else {
      return
    }
    
    let nodeLoade = NodeLoadOperation(graph: graph, node: node, searchable: searchable)
    
    nodeLoade.completionBlock = { [weak self] in
      self?.loadNodesWithBFS(initial: node)
    }
    operationQueue.addOperation(nodeLoade)
  }
  
  func stopProcess() {
    operationQueue.cancelAllOperations()
  }
  
  fileprivate func processNeighbor(neighbor: Node, explored: inout [String], operations: inout [NodeLoadOperation]) {
    if !neighbor.visited && explored.count < urlLimit {
      neighbor.visited = true
      self.nodeQueue?.enqueue(neighbor)
      explored.append(neighbor.url)
      let processOperation = NodeLoadOperation(graph: self.graph, node: neighbor, searchable: self.searchable)
      operations.append(processOperation)
    } else {
      self.nodeQueue?.clear()
    }
  }
  
  fileprivate func postNotification() {
    DispatchQueue.main.sync {
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: K.NodesAddedNotification), object: nil)
      if self.nodeQueue?.count == 0 {
        processFinished!()
      }
    }
  }
  
  func loadNodesWithBFS(initial: Node) {
    self.nodeQueue = Queue<Node>()
    self.nodeQueue?.enqueue(initial)
    var nodesExplored = [initial.url]
    initial.visited = true
    
    while !self.graph.isFull || nodeQueue!.count > 0 {
      guard let node = self.nodeQueue?.dequeue() else {
        return
      }
      
      var operations = [NodeLoadOperation]()
      
      for edge in node.neighbors {
        processNeighbor(neighbor: edge.neighbor, explored: &nodesExplored, operations: &operations)
      }
      operationQueue.addOperations(operations, waitUntilFinished: true)
      
      postNotification()
    }
  }
}
