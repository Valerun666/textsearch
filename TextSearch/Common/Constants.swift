//
//  Constants.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

struct K {
  static let UrlCell = "URLCell"
  static let ResultSegue = "ResultSegue"
  static let NodesAddedNotification = "NewNodesWereAdded"
}
