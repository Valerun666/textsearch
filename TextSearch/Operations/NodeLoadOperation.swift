//
//  NodeLoader.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/22/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit

class NodeLoadOperation: Operation {
  let graph: Graph
  let node: Node
  let searchable: String
  private let queue = DispatchQueue(label: "operation.queue")
  private var _executing = false
  private var _finished = false
  
  init(graph: Graph, node: Node, searchable: String) {
    self.graph = graph
    self.node = node
    self.searchable = searchable
    super.init()
  }
  
  override internal(set) var isExecuting: Bool {
    get {
      return _executing
    }
    set {
      if _executing != newValue {
        willChangeValue(forKey: "isExecuting")
        _executing = newValue
        didChangeValue(forKey: "isExecuting")
      }
    }
  }
  
  override internal(set) var isFinished: Bool {
    get {
      return _finished
    }
    set {
      if _finished != newValue {
        willChangeValue(forKey: "isFinished")
        _finished = newValue
        didChangeValue(forKey: "isFinished")
      }
    }
  }
  
  override var isAsynchronous: Bool {
    return true
  }
  
  override func main() {
    if self.isCancelled {
      isFinished = true
      return
    }
    
    isExecuting = true

    queue.async { [weak self] in
      guard let strongSelf = self else {
        print("*** NodeLoaderOperation *** Error")
        return
      }
      
      if strongSelf.isCancelled {
        strongSelf.isExecuting = false
        strongSelf.isFinished = true
      } else {
        strongSelf.performNodeProcess()
        strongSelf.isExecuting = false
        strongSelf.isFinished = true
      }
    }
  }
  
  func performNodeProcess() {
    let urlCandidat = URL(string: node.url)
    
    if urlCandidat == nil {
      node.setErrorStatus(errorStatus: "URL can't be loaded")
    } else {
      do {
        let contents = try String(contentsOf: urlCandidat!)
        let extracted = contents.extractURLStrings()
        node.status = contents.containsIgnoringCase(find: self.searchable) ? .Found : .NotFound
        
        if !graph.isFull {
          self.convertUrlsToChildNodes(neighbour: node, urlList: extracted)
        }
      } catch {
        node.setErrorStatus(errorStatus: error.localizedDescription)
        print("*** Catched Error : \(error.localizedDescription)")
      }
    }
  }
  
  func convertUrlsToChildNodes(neighbour: Node, urlList: [String]) {
    for urlString in urlList {
      let nodeToAdd = self.graph.addNode(urlString)
      
      guard let node = nodeToAdd else {
        return
      }
      
      self.graph.addEdge(neighbour, neighbor: node)
    }
  }
}
