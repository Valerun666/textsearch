//
//  StringExtensions.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/20/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import Foundation

extension String {
  func extractURLStrings() -> [String] {
    var urls : [String] = []
    let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)

    let matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))

    for match in matches {
      guard let range = Range(match.range, in: self) else { continue }
      let url = String(self[range])
      
      if url.contains(find: "http") {
        urls.append(url)
      }
    }

    return Array(Set(urls))
  }
  
  func contains(find: String) -> Bool{
    return self.range(of: find) != nil
  }
  
  func containsIgnoringCase(find: String) -> Bool{
    return self.range(of: find, options: .caseInsensitive) != nil
  }
}
