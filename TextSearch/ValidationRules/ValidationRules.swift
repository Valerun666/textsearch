//
//  ValidationRules.swift
//  TextSearch
//
//  Created by Valerii Teptiuk on 2/26/18.
//  Copyright © 2018 Valerii Teptiuk. All rights reserved.
//

import UIKit
import SwiftValidator

class URLRule: RegexRule {
  static let regex = "(?i)https?://(?:www\\.)?\\S+(?:/|\\b)"
  
  convenience init(message : String = "Not a valid URL"){
    self.init(regex: URLRule.regex, message : message)
  }
}

class ThreadRule: Rule {
  private var message : String

  public init(message : String = "Please provide thread quantity 2-9"){
    self.message = message
  }
  
  public func validate(_ value: String) -> Bool {
    if let number = Int(value) {
      return number > 0 && number <= 9
    }
    
    return false
  }
  
  public func errorMessage() -> String {
    return message
  }
}

class URLQuantityRule: Rule {
  private var message : String
  
  public init(message : String = "Please provide URL quantity > 0"){
    self.message = message
  }
  
  public func validate(_ value: String) -> Bool {
    if let number = Int(value) {
      return number != 0
    }
    
    return false
  }
  
  public func errorMessage() -> String {
    return message
  }
}

